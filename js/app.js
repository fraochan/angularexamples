var app = angular.module('WeatherApp', ['ngMaterial', 'ngRoute']);

app.config(function($routeProvider) {
  $routeProvider.when('/weather', {
    controller: 'WeatherController',
    controllerAs: 'ctrl',
    templateUrl: 'views/weather.html'
  }).when('/calendar', {
    controller: 'CalendarController',
    templateUrl: 'views/calendar.html'
  }).when('/', {
    templateUrl: 'views/home.html'
  }).otherwise({
    redirectTo: '/'
  })
  
});

app.controller('appCtrl',[ '$scope', '$mdSidenav', function($scope, $mdSidenav) {
  $scope.muestraMenu = function() {
    $mdSidenav('left').open();
  };
  $scope.ocultaMenu = function() {
    $mdSidenav('left').close();
  }
}]);