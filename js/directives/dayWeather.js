app.directive('dayWeather', [ function() {
	return {
		restrict: 'E',
		scope: {
			data: "=data"
		},
		templateUrl: 'js/directives/dayWeather.html'
	}
}]);