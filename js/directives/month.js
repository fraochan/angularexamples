app.directive('month', [ function() {
	
	return {
		restrict: 'E',
		scope: {
			number: "=number",
			year: "=year"
		},	
		templateUrl: 'js/directives/month.html',
		link: function(scope) {
			scope.$watch("year", function(newValue, oldValue) {
		          if (newValue !== oldValue) {
		            // You actions here
		            if (scope.numberOfDays(scope.number, scope.year) > 0) 
		            {
		            	scope.daysArray = Array.apply(0, Array(scope.numberOfDays(scope.number, scope.year))).map(function (x, y) { return y + 1; });
		            }
		            else
		            {
		            	scope.daysArray = [];
		            }
		            if (scope.initialDayOfMonth(scope.number, scope.year) > 0)
		            {
		    			scope.initialDays = Array.apply(0, Array(scope.initialDayOfMonth(scope.number, scope.year))).map(function (x, y) { return y; });
		            }
		            else
		            {
		            	scope.initialDays = [];
		            }
		          }
		      }, true);
			scope.getMonthName = function(monthNumber)
		    {
		        return monthNames[monthNumber-1];
		    }
		    scope.initialDayOfMonth = function(monthNumber, year)
		    {
		    	return (((new Date(year+"-"+monthNumber+"-1")).getDay()+6)%7);
		    }
		    scope.numberOfDays = function(monthNumber, year)
		    {
		    	if((monthNumber<=7 && (monthNumber%2)==0) || (monthNumber>7 && (monthNumber%2)!=0)) 
		    	{
		    		if (monthNumber==2) 
		    		{
		    			if(leapYear(year)) return 29;
		    			return 28;
		    		}
		    		return 30;
		    	}
		    	else
		    	{
		    		return 31;
		    	}
		    }
		    function leapYear(year)
		    {
		    	return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
		    }
		    if (scope.numberOfDays(scope.number, scope.year) > 0) 
		            {
		            	scope.daysArray = Array.apply(0, Array(scope.numberOfDays(scope.number, scope.year))).map(function (x, y) { return y + 1; });
		            }
		            else
		            {
		            	scope.daysArray = [];
		            }
		            if (scope.initialDayOfMonth(scope.number, scope.year) > 0)
		            {
		    			scope.initialDays = Array.apply(0, Array(scope.initialDayOfMonth(scope.number, scope.year))).map(function (x, y) { return y; });
		            }
		            else
		            {
		            	scope.initialDays = [];
		            }
		}
	}
}]);