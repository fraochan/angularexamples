app.controller('CalendarController', ['$scope','$window', function($scope, $window) {
    $scope.year = new Date().getFullYear(); // Current year
    $scope.months = Array.apply(0, Array(12)).map(function (x, y) { return y + 1; }); // Array from 1 to 12 to iterate over
    
    monthNames = ["January","February","March","April","May","June","July","August","September","October","November","December"];
    
    $scope.prevYear = function()
    {
        $scope.year--;
    }
    
    $scope.nextYear = function()
    {
        $scope.year++;
    }
    
    
}]);