app.controller('WeatherController', [ '$scope', 'forecast', 'locator', function($scope, forecast, locator) {

	$scope.units = "metric";

	var self = this;
	self.querySearch=querySearch;
	self.selectedItemChange = selectedItemChange;
	self.reloadForecast = reloadForecast;
	$scope.btnShow = false;
	$scope.buscando = false;
	$scope.mapImages = {
		"200" : "11d",		"201" : "11d",		"202" : "11d",		"210" : "11d",
		"211" : "11d",		"212" : "11d",		"221" : "11d",		"230" : "11d",
		"231" : "11d",		"232" : "11d",		"300" : "09d",		"301" : "09d",
		"302" : "09d",		"310" : "09d",		"311" : "09d",		"312" : "09d",
		"313" : "09d",		"314" : "09d",		"321" : "09d",		"500" : "10d",
		"501" : "10d",		"502" : "10d",		"503" : "10d",		"504" : "10d",
		"511" : "13d",		"520" : "09d",		"521" : "09d",		"522" : "09d",
		"531" : "09d",		"600" : "13d",		"601" : "13d",		"602" : "13d",
		"611" : "13d",		"612" : "13d",		"615" : "13d",		"616" : "13d",
		"620" : "13d",		"621" : "13d",		"622" : "13d",		"701" : "50d",
		"711" : "50d",		"721" : "50d",		"731" : "50d",		"741" : "50d",
		"751" : "50d",		"761" : "50d",		"762" : "50d",		"771" : "50d",
		"781" : "50d",		"800" : "01d",		"801" : "02d",		"802" : "03d",
		"803" : "04d",		"804" : "04d"
	};
	
	locator.success(function(data) {
		if (data.cod != 200) {
			return null;
		}
		$scope.selectedItem = {
			value: data.city.replace(/\s/g, "_")+","+data.country_code.replace(/\s/g, "_"),
			display: ""
		};
		reloadForecast()
	})
	
	function querySearch(searchText) 
	{
		var request = {
			input: searchText
		}
		var service = new google.maps.places.AutocompleteService();
		service.getPlacePredictions(request, function(data, status) {
			if (status == google.maps.places.PlacesServiceStatus.OK) {
				$scope.cities = data.map(function(city) {
					var valor;
					if (city.terms.length>1)
					{
						valor="";
						city.terms.forEach(function(term) {
							if (term.value.indexOf(searchText.substring(0,2))>=0) 
							{
								valor = term.value;
							}
						});
						if (valor=="")
							valor = city.terms[city.terms.length-2].value+","+city.terms[city.terms.length-1].value;
						else
							valor += ","+city.terms[city.terms.length-1].value;
					}
					else
					{
						valor = city.terms[city.terms.length-1].value;
					}
					return {
						value: valor,
						display: city.description,
						terms: city.terms,
						type: city.type
					}
				});
			}
		});
		return $scope.cities;
	};
	
	function reloadForecast()
	{
		$scope.buscando=true;
		if ($scope.selectedItem && $scope.selectedItem.value !="") 
		{
			forecast(replaceDiacritics($scope.selectedItem.value), $scope.units).success(function(data) 
			{
				$scope.forecast = prepareForecast(data);
				if ($scope.forecast != null) 
				{
					$scope.btnShow = true;
				} 
				else
				{
					$scope.forecast = {
						name: "City not found",
						country: "try again",
						list: []
					}
				}
				$scope.buscando = false;
			});
		} else {
			$scope.buscando = false;
		}
	}
	
	/*function isANumber ( obj ) {
    return !jQuery.isArray( obj ) && (obj - parseFloat( obj ) + 1) >= 0;
	}*/
	
	$scope.toggleUnits = function ()
	{
		if($scope.forecast) {
			$scope.units = $scope.units == "metric" ? "imperial" : "metric";
			$scope.forecast.list.forEach(function(elem) 
			{
				if (elem.units == "C") 
				{
					elem.units = "F";
					elem.main.temp_min = !isNaN(elem.main.temp_min) ? (elem.main.temp_min * 9.0 / 5.0 + 32).toFixed(2) : elem.main.temp_min;
					elem.main.temp_max = !isNaN(elem.main.temp_max) ? (elem.main.temp_max * 9.0 / 5.0 + 32).toFixed(2) : elem.main.temp_max;
				}
				else
				{
					elem.units = "C";
					elem.main.temp_min = !isNaN(elem.main.temp_min) ? ((elem.main.temp_min - 32) * 5.0/9.0).toFixed(2) : elem.main.temp_min;
					elem.main.temp_max = !isNaN(elem.main.temp_max) ? ((elem.main.temp_max - 32) * 5.0/9.0).toFixed(2) : elem.main.temp_max;
				}
			})
		}
	}
	
	function selectedItemChange(item) 
	{
		if (item) {
			item.value = item.value.replace(/\s/g, "_");
			$scope.selectedItem = item;
			//$scope.forecast = {};
			$scope.btnShow = false;
			reloadForecast();
		}
	}
	
	function prepareForecast(data) 
	{
		var res = {};
		
		if (data.cod != 200) {
			return null;
		}
		res = { 
			name: data.city.name,
			country: data.city.country,
			list: []
		};
		var current;
		var calcularMinima = true;
		data.list.forEach(function(elem) 
		{
			if(!current) 
			{
				current = nuevoElemento(elem);
				res.list.push(current);
				if (new Date(current.dt).getHours()>6) {
					calcularMinima = false;
					current.main.temp_min = "-";
				} 
			} 
			else 
			{
				if (new Date(current.dt).getDate()!=new Date(elem.dt*1000).getDate()) 
				{
					if (res.list.indexOf(current)<0) res.list.push(current);
					current = nuevoElemento(elem);
					calcularMinima = true;
				} 
				else
				{
					if(elem.main.temp_min < current.main.temp_min && calcularMinima) {
						current.main.temp_min = elem.main.temp_min;
					}
					if(elem.main.temp_max > current.main.temp_max) {
						current.main.temp_max = elem.main.temp_max;
					}
				}
			}
		})
		if (res.list.indexOf(current)<0) res.list.push(current);
		return res;
	}
	
	function nuevoElemento(elem) 
	{
		return {
			dt: elem.dt*1000,
			weather: elem.weather,
			img: $scope.mapImages[elem.weather[0].id],
			main: elem.main,
			units: $scope.units == "metric" ? "C" : "F",
			diaActual: new Date().getDate() == new Date(elem.dt*1000).getDate() ? "diaActual" : ""
		}
	}
	
	function replaceDiacritics(s)
	{
	    var s;
	
	    var diacritics =[
	        /[\300-\306]/g, /[\340-\346]/g,  // A, a
	        /[\310-\313]/g, /[\350-\353]/g,  // E, e
	        /[\314-\317]/g, /[\354-\357]/g,  // I, i
	        /[\322-\330]/g, /[\362-\370]/g,  // O, o
	        /[\331-\334]/g, /[\371-\374]/g,  // U, u
	        /[\321]/g, /[\361]/g, // N, n
	        /[\307]/g, /[\347]/g, // C, c
	    ];
	
	    var chars = ['A','a','E','e','I','i','O','o','U','u','N','n','C','c'];
	
	    for (var i = 0; i < diacritics.length; i++)
	    {
	        s = s.replace(diacritics[i],chars[i]);
	    }
	
	    return s;
	}

}]);