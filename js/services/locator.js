app.factory('locator', [ '$http', function($http) {
    return $http.jsonp('https://www.telize.com/geoip?callback=JSON_CALLBACK').
        success(function(data) {
            return data;
        }).error(function(err) {
            return err;
        })
}])