app.factory('forecast', [ '$http', function($http) {
	return function(city, unit) {
		return $http.jsonp('https://cors.5apps.com/?uri=http://api.openweathermap.org/data/2.5/forecast?callback=JSON_CALLBACK&lang=es&APPID=055cb69770a5f1fdbab4db4943473df6&q=' + city + '&units=' + unit).
		success(function(data) {
			return data;
		}).
		error(function(err) {
			return err;
		});
	}
}])