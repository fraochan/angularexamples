app.factory('cities', [ '$http', function($http) {
	return function(par1) {
		return $http.jsonp('http://gd.geobytes.com/AutoCompleteCity?callback=JSON_CALLBACK&q=' + par1).
		success(function(data) {
			return data;
		}).
		error(function(err) {
			return err;
		});
	}
}])